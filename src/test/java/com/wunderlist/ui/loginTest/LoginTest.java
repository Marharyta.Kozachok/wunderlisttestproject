package com.wunderlist.ui.loginTest;

import com.wunderlist.pageObjectClasses.wunderlistPages.LoginPage;
import com.wunderlist.ui.BaseTest;
import org.testng.annotations.Test;

import static com.wunderlist.helpers.DataGenerator.*;

public class LoginTest extends BaseTest {
    LoginPage loginPageObj = new LoginPage();

    @Test(description = "Test login user with non exiting userData")
    public void testLoginUserWithNonExitingUserData() {
        loginPageObj
                .openLoginPage()
                .loginUser(getFakerEmailAddress(), getFakerPassword(8, 16))
                .clickSubmitButton()
                .shouldHaveErrorMessage("Неправильный адрес электронной почты или пароль. Попробуйте еще раз.");
    }

    @Test(description = "Test login user with invalid Email")
    public void testLoginUserWithInvalidEmail() {
        loginPageObj
                .openLoginPage()
                .loginUser(getFakerUserName(), getFakerPassword(8, 16))
                .clickSubmitButton()
                .shouldHaveLoginAndPassFieldIsVisible();
    }

    @Test(description = "Test login user with empty password")
    public void testLoginUserWithEmptyPassword() {
        loginPageObj
                .openLoginPage()
                .loginUser(getFakerUserName(), "")
                .clickSubmitButton()
                .shouldHaveLoginAndPassFieldIsVisible();
        ;
    }

    @Test(description = "Test login user using FaceBook")
    public void testLoginUserUsingFaceBook() {
        loginPageObj
                .openLoginPage()
                .clickFaceBookButton()
                .shouldHaveHeaderText("Вход на Facebook");
    }

    @Test(description = "Test login user using Microsoft Account")
    public void testLoginUserUsingMicrosoftAccount() {
        loginPageObj
                .openLoginPage()
                .clickMicrosoftButton()
                .clickMicrosoftAccountButton()
                .shouldHaveHeaderText("Вход");
    }

    @Test(description = "Test login user using Work or School Account")
    public void testLoginUserUsingWorkOrSchoolAccount() {
        loginPageObj
                .openLoginPage()
                .clickMicrosoftButton()
                .clickWorkSchoolAccountButton()
                .shouldHaveHeaderText("Войти");
    }

    @Test(description = "Test forgot password button")
    public void testForgotPasswordButton() {
        loginPageObj
                .openLoginPage()
                .clickForgotPasswordButton()
                .shouldHaveLabelText("Забыли пароль?");
    }

}
