package com.wunderlist.pageObjectClasses.wunderlistPages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class ForgotPasswordPage {

    private SelenideElement forgotPasswordText = $(By.cssSelector("[class='row labels'] h2"));

    private SelenideElement forgotEmailInput = $(By.cssSelector("[name='email']"));
    private SelenideElement forgotPasswordButton = $(By.cssSelector("[value='Восстановить пароль']"));


    private SelenideElement errorMessage = $(By.cssSelector("[role='alert']"));

    public ForgotPasswordPage openForgotPasswordPage(){
        open("https://www.wunderlist.com/forgotpassword");

        return this;
    }

    public ForgotPasswordPage clickForgotButton(){
        forgotPasswordButton.click();

        return this;
    }

    public ForgotPasswordPage fillInForgotEmailInput(String email){
        forgotEmailInput.setValue(email);

        return this;
    }

    public ForgotPasswordPage shouldHaveLabelText(String text){
        forgotPasswordText.shouldHave(Condition.exactText(text));

        return this;
    }

    public ForgotPasswordPage shouldHaveErrorMessage(String text){
        errorMessage.shouldHave(Condition.exactText(text));

        return this;
    }



}
