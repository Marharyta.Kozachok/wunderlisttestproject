package com.wunderlist.pageObjectClasses.microsoftLoginPage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class MicrosoftLoginPage {

    SelenideElement headerText = $(By.id("loginHeader")).$(By.cssSelector("[role='heading']"));

    public MicrosoftLoginPage shouldHaveHeaderText(String text){
        headerText.shouldHave(Condition.exactText(text));

        return this;
    }
}
