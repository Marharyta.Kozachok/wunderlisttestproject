package com.wunderlist.helpers;

import com.github.javafaker.Faker;

public class DataGenerator {

    private static Faker faker = new Faker();

    public static String getFakerUserName(){
        return faker.name().username().replace(".", "");
    }

    public static String getFakerEmailAddress(){
        return faker.internet().emailAddress();
    }

    public static String getFakerPassword(int minimumLength, int maximumLength){
        return faker.internet().password();
    }
}
