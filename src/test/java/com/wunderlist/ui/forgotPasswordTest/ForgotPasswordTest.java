package com.wunderlist.ui.forgotPasswordTest;

import com.wunderlist.pageObjectClasses.wunderlistPages.ForgotPasswordPage;
import com.wunderlist.ui.BaseTest;
import org.testng.annotations.Test;

import static com.wunderlist.helpers.DataGenerator.getFakerEmailAddress;

public class ForgotPasswordTest extends BaseTest {

    ForgotPasswordPage forgotPasswordPage = new ForgotPasswordPage();

    @Test(description = "Test login user with non exiting userData")
    public void testUserForgotPassword() {
        forgotPasswordPage
                .openForgotPasswordPage()
                .fillInForgotEmailInput(getFakerEmailAddress()).clickForgotButton()
                .shouldHaveErrorMessage("Не удалось найти учетную запись с этим адресом электронной почты. Для получения справки посетите support.wunderlist.com.");
    }
}
