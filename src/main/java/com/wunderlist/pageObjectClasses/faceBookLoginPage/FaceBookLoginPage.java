package com.wunderlist.pageObjectClasses.faceBookLoginPage;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class FaceBookLoginPage {


    SelenideElement headerText = $(By.cssSelector("[id='header_block'] span"));

    public FaceBookLoginPage shouldHaveHeaderText(String text){
        headerText.shouldHave(Condition.matchesText(text));

        return this;
    }

}
