package com.wunderlist.pageObjectClasses.wunderlistPages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.wunderlist.pageObjectClasses.faceBookLoginPage.FaceBookLoginPage;
import com.wunderlist.pageObjectClasses.microsoftLoginPage.MicrosoftLoginPage;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage {

    private SelenideElement loginInput = $(By.cssSelector("[name='email']"));
    private SelenideElement passwordInput = $(By.cssSelector("[name='password']"));
    private SelenideElement submitButton = $(By.cssSelector("[type='submit']"));
    private SelenideElement forgotPasswordButton = $(By.cssSelector("[class='link forgot-password'] a"));

    private SelenideElement faceBookButton = $(By.cssSelector("[class='icon facebook']"));
    private SelenideElement googlePlusButton = $(By.cssSelector("[class='link forgot-password'] a"));

    private SelenideElement microsoftButton = $(By.cssSelector("[class='external-auth microsoft-selector'] a"));
    private SelenideElement microsoftAccountButton = $(By.cssSelector("[class='external-auth microsoft'] a"));
    private SelenideElement workSchoolAccountButton = $(By.cssSelector("[class='external-auth aad'] a"));

    private SelenideElement createUserButton = $(By.cssSelector("[class='link signup'] a"));

    private SelenideElement errorMessage = $(By.cssSelector("[role='alert']"));



    public LoginPage openLoginPage(){
        open("https://www.wunderlist.com/login");

        return this;
    }

    public LoginPage loginUser(String email, String pass){
        loginInput.setValue(email);
        passwordInput.setValue(pass);

        return this;
    }

    public LoginPage clickSubmitButton(){
        submitButton.click();

        return this;
    }

    public ForgotPasswordPage clickForgotPasswordButton(){
        forgotPasswordButton.click();

        return new ForgotPasswordPage();
    }


    public FaceBookLoginPage clickFaceBookButton(){
        faceBookButton.click();

        return new FaceBookLoginPage();
    }

    public LoginPage clickMicrosoftButton(){
        microsoftButton.click();

        return this;
    }

    public MicrosoftLoginPage clickMicrosoftAccountButton(){
        microsoftAccountButton.click();

        return new MicrosoftLoginPage();
    }

    public MicrosoftLoginPage clickWorkSchoolAccountButton(){
        workSchoolAccountButton.click();

        return new MicrosoftLoginPage();
    }

    public LoginPage shouldHaveLoginAndPassFieldIsVisible(){
        loginInput.shouldHave(Condition.visible);
        passwordInput.shouldHave(Condition.visible);

        return this;
    }

    public LoginPage shouldHaveErrorMessage(String errorMessageText){
        errorMessage.shouldHave(Condition.matchesText(errorMessageText));

        return this;
    }

}
